@extends('layout.master')

@section('judul')
Register Form
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <fieldset>
    {{-- <legend>Register Form</legend> --}}
    <label for="fName"> First name : </label><br />
    <input type="text" placeholder="first name" name="fname" id="fname" /><br />
    <br />
    <label for="lname"> Last name : </label><br />
    <input type="text" name="lname" id="lname" placeholder="last name" /><br />
    <br />
    <label for="Gender"> Gender </label><br />
    <input type="radio" name="Gender" value="Male" />Male <br />
    <input type="radio" name="Gender" value="Female" />Female <br /><br />
    <label for="Nationality">Nationality</label><br />
    <select name="Nationality" id="">
    <optgroup label="PBSI">
        <option value="Pakistan">Pakistan</option>
        <option value="Bangladesh">Bangladesh</option>
        <option value="Srilangka">Srilangka</option>
        <option value="India">India</option>
        </optgroup>
        <optgroup label="Asia Tenggara">
        <option value="Indonesia">Indonesia</option>
        <option value="Singapura">Singapura</option>
        <option value="DBS">Planet namek</option>
        </optgroup>
    </select><br /><br />
    <label for="Language">Language Spoken</label><br />
    <input type="checkbox" name="Language" value="Bahasa Indonesia"  /> Bahasa Indonesia
    {{-- checked tambahahan di checkbox value kok 0 ??--}}
    <br />
    <input type="checkbox" name="Language" value="English" /> English <br />
    <input type="checkbox" name="Language" value="Other" /> Other <br />
    <label for="Bio">Bio</label><br />
    <textarea name="Bio" cols="30" rows="10"></textarea><br />
    <input type="submit" value="Sign Up" />
    </fieldset>
</form>
@endsection
