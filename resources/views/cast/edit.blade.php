@extends('layout.master')

@section('judul')
Edit cast
@endsection

@section('content')
<h2>Edit Cast {{$cast->id}}</h2>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
<div class="form-group">
    <label >Nama Lengkap</label>
    <input type="text" name="nama" class="form-control" value="{{$cast->nama}}" >
</div>

@error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Umur</label>
    <input type="text" name="umur" class="form-control" value="{{$cast->umur}}" >
</div>
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label >Biodata</label>
    <textarea name="bio" class="form-control">{{$cast->bio}}</textarea><br />
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection