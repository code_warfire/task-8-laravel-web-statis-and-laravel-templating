@extends('layout.master')

@section('judul')
<h1>Index Casting / Caster</h1>
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah caster</a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Lengkap</th>
      <th scope="col">Umur</th>
      <th scope="col">Biodata</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($cast as $key => $item)
          <tr>
              <td>{{$key + 1}} </td>
              <td>{{$item->nama}} </td>
              <td>{{$item->umur}} </td>
              <td>{{$item->bio}} </td>
              <td>
                <form action="/cast/{{$item->id}}" method="post">
                  <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>                
                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
              </td>
          </tr>
      @empty
          <h1>Data not found</h1>
      @endforelse
  </tbody>
</table>
@endsection