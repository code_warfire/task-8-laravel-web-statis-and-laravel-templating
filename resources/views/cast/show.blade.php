@extends('layout.master')

@section('judul')
Register Form
@endsection

@section('content')
<h1>No {{$cast->id}}</h1>
<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-success ms-3">HOME</a>
@endsection