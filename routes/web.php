<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/hello', function ()
// {
//     return "Hello World";
// });

// use Illuminate\Routing\Route;
// use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@home');

Route::get('/data-tables', 'IndexController@tables');

Route::get('/Register', 'AuthController@Register');

Route::post('/welcome','AuthController@welcome');

// CRUD cast
// Create
Route::get('/cast/create', 'CastController@create'); //menampilkan form untuk membuat data pemain film baru
Route::post('/cast', 'CastController@store'); //menyimpan data baru ke tabel Cast

// Read
Route::get('/cast', 'CastController@index');//menampilkan list data para pemain film
Route::get('/cast/{cast_id}', 'CastController@show');//menampilkan detail data pemain film dengan id tertentu

// Update
Route::get('/cast/{cast_id}/edit','CastController@edit'); //route untuk menuju form edit 
Route::put('/cast/{cast_id}','CastController@update'); //route untuk update data based on id

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route untuk menghapus data di DB

// Route::get('/master', function()
// {
//     return view('layout.master');
// });
