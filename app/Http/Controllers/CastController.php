<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
class CastController extends Controller
{
    // menampilkan form page
    public function create(){
        return view('cast.create');
    }
    // function push data dari form page terpush ke DB engine
    public function store(Request $request){
        //dd($request->all()); //check bypass inputan terarray
        $validatedData = $request->validate(
            [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = new cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio  = $request->bio;
        $cast->save();

        return redirect('/cast');
    }

    // mengambil data dari database
    public function index()
    {
        $cast = cast::all();
        // dd($cast); //bypass check array DB 
        return view('cast.index', compact('cast'));
    } 
    
    // fungsi mengambil detail casting based on $cast_id
    public function show($cast_id){
        $cast = cast::find($cast_id);
        return view('cast.show', compact('cast'));
    }
    // fungsi mengedit detail casting based on $cast_id
    public function edit($cast_id){
        $cast = cast::find($cast_id);
        return view('cast.edit', compact('cast'));
    }
    // fungsi mengupdate data yang di edit / Post data
    public function update (Request $request, $cast_id){
        $validatedData = $request->validate(
            [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);        

        $cast = cast::find($cast_id);
 
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];
        
        $cast->save();
        return redirect('/cast');
    }
    // fungsi menghapus data 
    public function destroy($cast_id){
        $cast = cast::find($cast_id);
        $cast->delete();

        return redirect('/cast');
    }
}