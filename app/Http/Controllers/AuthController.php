<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('HTML.Register');
    }
    public function welcome(Request $request){
        //dd($request->all());
        $fname = $request['fname'];
        $lname = $request['lname'];
        $Gender = $request['Gender'];
        $Nationality = $request['Nationality'];
        $Language = $request['Language'];
        $Bio = $request['Bio'];

        return view('HTML.welcome', compact('fname','lname','Gender','Nationality','Language','Bio'));

    }
}